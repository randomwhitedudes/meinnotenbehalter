package com.finbot.noten;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created on 03.02.2016.
 * UI Class to handle the users input
 * @author Martin Fink
 */
public class Window extends JFrame {

    private JTextField grade;
    private JTextField subject;
    private JTextField type;
    private JTextField date;

    private GradeList gradeList;
    private boolean cache_last = false; // if there is already a last, empty grade, this variable is true

    /**
     * Standard constructor which generates a new instance of the GradeList class
     * Checks for errors in the .csv file
     * Sets onClickListeners for each buttons
     */
    public Window() {
        setTitle("Grades");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(20, 20, 400, 320);
        setResizable(false);

        JLabel grade_l = new JLabel("Grade:");
        grade_l.setBounds(20, 20, 100, 30);
        JLabel subject_l = new JLabel("Subject:");
        subject_l.setBounds(20, 60, 100, 30);
        JLabel type_l = new JLabel("Type:");
        type_l.setBounds(20, 100, 100, 30);
        JLabel date_l = new JLabel("Date:");
        date_l.setBounds(20, 140, 100, 30);

        grade = new JTextField();
        grade.setBounds(120, 20, 240, 30);
        subject = new JTextField();
        subject.setBounds(120, 60, 240, 30);
        type = new JTextField();
        type.setBounds(120, 100, 240, 30);
        date = new JTextField();
        date.setBounds(120, 140, 240, 30);

        JButton first = new JButton();
        first.setBounds(20, 180, 70, 30);
        first.setText("First");
        JButton previous = new JButton();
        previous.setBounds(110, 180, 70, 30);
        previous.setText("Previous");
        JButton next = new JButton();
        next.setBounds(200, 180, 70, 30);
        next.setText("Next");
        JButton last = new JButton();
        last.setBounds(290, 180, 70, 30);
        last.setText("Last");

        JButton add = new JButton();
        add.setBounds(20, 230, 90, 30);
        add.setText("Add");
        JButton delete = new JButton();
        delete.setBounds(145, 230, 90, 30);
        delete.setText("Delete");
        JButton delAll = new JButton();
        delAll.setBounds(270, 230, 90, 30);
        delAll.setText("Delete All");

        Container container = getContentPane();
        container.setLayout(null);
        container.add(grade);
        container.add(grade_l);
        container.add(subject);
        container.add(subject_l);
        container.add(type);
        container.add(type_l);
        container.add(date);
        container.add(date_l);
        container.add(first);
        container.add(previous);
        container.add(next);
        container.add(last);
        container.add(add);
        container.add(delete);
        container.add(delAll);

        setVisible(true);

        try {
            gradeList = new GradeList();
            showErrorMessage("Before closing the program, please\nswitch to another grade to save the file!");
        } catch (ParseException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(getContentPane(), "Error parsing the dates\nPlease check your .csv file and correct the dates");
            System.exit(-1);
        }

        if (gradeList.containsValues()) {
            setContent(gradeList.getFirst());
        }

        first.addActionListener(e -> {
            saveGrade();
            if (gradeList.containsValues()) {
                setContent(gradeList.getFirst());
            } else {
                showErrorMessage("No grades found!\ntry adding one!");
            }
         });

        previous.addActionListener(e -> {
            saveGrade();
            if (gradeList.containsValues()) {
                if (gradeList.hasPrevious()) {
                    setContent(gradeList.getPrevious());
                } else {
                    showErrorMessage("You are already at the first position!");
                }
            } else {
                showErrorMessage("No grades found!\nTry adding one!");
            }
            if (cache_last) {
                if (gradeList.getLast().isEmpty()) {
                    cache_last = false;
                }
            }
        });

        next.addActionListener(e -> {
            saveGrade();
            if (!gradeList.getLast().isEmpty() || gradeList.hasNext()) {
                cache_last = false;
                if (gradeList.containsValues() && gradeList.hasNext() && !cache_last) {
                    setContent(gradeList.getNext());
                } else {
                    addGrade();
                }
            }
        });

        last.addActionListener(e -> {
            saveGrade();
            gradeList.removeUnused();
            if (gradeList.containsValues() && !cache_last) {
                setContent(gradeList.getLast());
            } else {
                gradeList.setPosCurrent(gradeList.getLastPos() - 1);
                setContent(gradeList.getCurrent());
            }
        });

        add.addActionListener(e -> {
            saveGrade();
            addGrade();
        });

        delete.addActionListener(e -> {
            saveGrade();
            showConfirmDeleteDialog("Do you really want to delete this grade?", "Delete grade", false);
            Grade temp = gradeList.getPrevious();
            if (temp != null) {
                setContent(temp);
            } else {
                gradeList.setPosCurrent(gradeList.getPosCurrent() + 1);
                temp = gradeList.getNext();
                if (temp != null) {
                    setContent(temp);
                } else {
                    addGrade();
                }
            }
        });

        delAll.addActionListener(e -> {
            showConfirmDeleteDialog("Do you really want to delete all grades?\nIf you do this, all saved grades are gone", "Delete all", true);
            if (gradeList.containsValues()) {
                addGrade();
            }
            else {
                addGrade();
            }
        });

    }

    /**
     * Shows a ConfirmDialog and waits for an user input
     * @param message description to show the user what the function does
     * @param title title of the dialog
     * @param delAll option if all the files get deleted (true) or just the current(false)
     */
    private void showConfirmDeleteDialog(String message, String title, boolean delAll) {
        int reply = JOptionPane.showConfirmDialog(getContentPane(), message, title, JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            if (delAll) {
                gradeList.removeAll();
            } else {
                gradeList.removeGrade();
            }
        }
    }

    /**
     * Shows a dialog to inform the user about errors and warnings
     * @param message Message which gets displayed
     */
    private void showErrorMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }

    /**
     * Sets the content of the textFields to the values of a grade class
     * @param grade which gets displayed
     */
    private void setContent(Grade grade) {
        this.grade.setText(grade.getGrade());
        this.subject.setText(grade.getSubject());
        this.type.setText(grade.getType());
        this.date.setText(grade.getDate());
    }

    /**
     * Method which gets called if the user wants to add a new grade
     */
    private void addGrade() {
        gradeList.removeUnused();
        if (!cache_last) {
            gradeList.addGrade();
            setContent(gradeList.getLast());
            gradeList.setPosCurrent(gradeList.getLastPos());
            cache_last = true;
        }
    }

    /**
     * Method to save the grade into the gradeList
     * The grade does get saved into the .csv file
     */
    private void saveGrade() {
        try {
            gradeList.saveGrade(new Grade(grade.getText(), type.getText(), subject.getText(), Grade.DATE_FORMAT.parse(date.getText())));
            gradeList.saveAll();
        } catch (ParseException e) {
            showErrorMessage("Wrong date format!\nUse yyyy-MM-dd instead");
        } catch (FileNotFoundException e) {
            showErrorMessage("File not found!");
        } catch (IOException e) {
            showErrorMessage("IO-Error!");
            e.printStackTrace();
        }
    }
}
