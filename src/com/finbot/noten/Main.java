package com.finbot.noten;

/**
 * Main class
 * Launches the other classes
 * @author Martin Fink
 */
public class Main {

    public static void main(String[] args) {
        new Window();
    }
}
