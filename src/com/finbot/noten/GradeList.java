package com.finbot.noten;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Class to work with the grades in a list
 * Created on 07.02.2016.
 * @author Andreas Botzner
 */
public class GradeList {
    private ArrayList<Grade> grades = new ArrayList<>();
    private String path = ".\\grades.csv";
    private int posCurrent = 0;

    /**
     * Custom constructor that creates a array list with Grade elements.
     * The array list will be sorted with a Sort By Minimum Search algorythm
     *
     * @throws ParseException
     */
    public GradeList() throws ParseException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line = reader.readLine();
            do {
                grades.add(new Grade(line));
                line = reader.readLine();
            } while (line != null);
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Reading Error");
        }
        for (int i = 0; i < grades.size(); i++) {
            for (int j = 0; j < grades.size() ; j++) {
                if(grades.get(i).compareTo(grades.get(j)) < 0){
                    Grade buffer;
                    buffer  = grades.get(i);
                    grades.set(i, grades.get(j));
                    grades.set(j, buffer);
                }
            }
        }
    }

    /** Gets the next Grade in the array list,
     *  based on the current grade and on the fact that there is a next gade.
     *
     * @return Returns the next Grade in the array list
     */
    public Grade getNext() {
        if (hasNext()) {
            posCurrent++;
            return grades.get(posCurrent);
        } else {
            Grade grade = new Grade();
            grades.add(grade);
            posCurrent++;
            return grade;
        }
    }

    /** Gets the previous Grade in the array list,
     *  based on the current grade and on the fact that there is a previous gade.
     *
     * @return Returns the previous Grade in the array list
     */
    public Grade getPrevious() {
        if (hasPrevious()) {
            posCurrent--;
            return grades.get(posCurrent);
        } else {
            return null;
        }
    }

    /** Gets the first Grade in the array list,
     *  based on the current grade.
     *
     * @return Returns the first Gradein the array list
     */
    public Grade getFirst() {
        posCurrent = getFirstPos();
        return (posCurrent < 0) ? null : grades.get(posCurrent);
    }

    /**
     * Gets the last Grade in the array list,
     * based on the current grade.
     *
     * @return last grade in the array list
     */
    public Grade getLast() {
        int pos = getLastPos();
        return (pos < 0) ? null : grades.get(pos);
    }

    /**
     * Method sets current position in the array list
     * @param posCurrent current position
     */
    public void setPosCurrent(int posCurrent) {
        this.posCurrent = posCurrent;
    }

    /**
     * Method gets current position in the array list
     *
     * @return current position
     */
    public int getPosCurrent() {
        return posCurrent;
    }

    /**
     * Method gets the position of the last element in the array list
     *
     * @return position of the last element
     */
    public int getLastPos() {
        return (grades.isEmpty()) ? -1 : grades.size() - 1;
    }

    /**
     * Method gets the position of the first element in the array list
     *
     * @return position of the first element
     */
    public int getFirstPos() {
        return (grades.isEmpty()) ? -1 : 0;
    }

    /**
     * Method checks if there is a next element in the array list
     *
     * @return true if there is a next element, fale if there is none
     */
    public boolean hasNext() {
        return posCurrent < grades.size() - 1;
    }

    /**
     * Method checks if there is a previous element in the array list
     *
     * @return true if there is a previous element, false if there is none
     */
    public boolean hasPrevious() {
        return posCurrent > 0;
    }

    /**
     * Method checks if there are elements in the array list
     *
     * @return true if there is/are elements, fale if there is none
     */
    public boolean containsValues() {
        return !grades.isEmpty();
    }

    /**
     * Method adds a new grade to the array list
     *
     */
    public void addGrade() {
        Grade grade = new Grade();
        grades.add(grade);
    }

    /**
     * Method removes the current element from the array list
     *
     */
    public void removeGrade() {
        grades.remove(posCurrent);
    }

    /**
     * method removes the element in the array list at a exact possition
     *
     * @param position position of the element that will be removed
     */
    public void removeGrade(int position) {
        grades.remove(position);
    }

    /**
     * Method removes all elements from the array list
     *
     */
    public void removeAll() {
        grades.clear();
        posCurrent = 0;
    }

    /**
     * Is used to save a grade in the Array list before changing the displayed grade
     *
     * @param grade grade with the new values that need to be saved in the array list
     */
    public void saveGrade(Grade grade) throws ParseException {
        grades.get(posCurrent).changeValues(grade);
    }

    /**
     * method gets the current position in the array list
     *
     * @return current position in the array list
     */
    public Grade getCurrent() {
        return grades.get(posCurrent);
    }

    /**
     * Removes unused grades from the list
     * grades are unused, if there is only the date saved in them
     */
    public void removeUnused() {
        Grade cache;
        for (int i = grades.size() - 1; i > 0; i--) {
            cache = grades.get(i);
            if (cache.isEmpty()) {
                removeGrade(getLastPos());
            } else {
                break;
            }
        }
        posCurrent = getLastPos();
    }

    /**
     * Method saves all elements from the array list in the grades.csv file
     *
     * @throws IOException
     */
    public void saveAll() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        for (Grade grade : grades) {
            writer.write(grade.toString() + "\n");
        }
        writer.close();
    }
}
