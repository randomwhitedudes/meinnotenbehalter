package com.finbot.noten;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created on 04.02.2016.
 * Class to save the grades
 * Contains multiple methods and the date format
 * @author Martin Fink
 */
public class Grade {
    private String grade;
    private String type;
    private String subject;
    private Date date;
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Standard Constructor which creates an empty class with todays date
     */
    public Grade() {
        this.grade = "";
        this.type = "";
        this.subject = "";
        try {
            this.date = DATE_FORMAT.parse(DATE_FORMAT.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Non-standard constructor to create an instance with values in it
     * @param grade grade
     * @param type type
     * @param subject subject
     * @param date date - has to be in the standard format
     */
    public Grade(String grade, String type, String subject, Date date) {
        this.grade = grade;
        this.type = type;
        this.subject = subject;
        this.date = date;
    }

    /**
     * Parses a string to a grade object
     *
     * @param grade a string read from a csv file
     * @throws ParseException
     */
    public Grade(String grade) throws ParseException {
        int index = grade.indexOf(';');
        this.date = DATE_FORMAT.parse(grade.substring(0, index));
        grade = grade.substring(index + 1, grade.length());
        index = grade.indexOf(';');
        this.subject = grade.substring(0, index);
        grade = grade.substring(index + 1, grade.length());
        index = grade.indexOf(';');
        this.type = grade.substring(0, index);
        grade = grade.substring(index + 1, grade.length());
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public String getType() {
        return type;
    }

    public String getSubject() {
        return subject;
    }

    public String getDate() {
        return DATE_FORMAT.format(this.date);
    }

    /**
     * Method to check if a grade equals another one
     *
     * @param grade grade which needs to be checked
     * @return boolean if the object is a grade and if it equals this grade
     */
    @Override
    public boolean equals(Object grade) {
        if (!getClass().getName().equals(grade.getClass().getName())) {
            return false;
        }
        Grade grade1 = (Grade) grade;
        try {
            if (!this.grade.equals(grade1.getGrade()) || !this.date.equals(DATE_FORMAT.parse(grade1.getDate()))
                    || !this.type.equals(grade1.getType()) || !this.subject.equals(grade1.getSubject()))
                            return false;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Method to compare two grades
     * The variables get checked in the following order:
     * date, subject, type, grade
     *
     * @param grade which needs to be compared
     * @return int difference
     */
    public int compareTo(Grade grade) {
        int diff = 0;
        try {
            diff = date.compareTo(DATE_FORMAT.parse(grade.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (diff != 0)
            return diff;
        diff = subject.compareTo(grade.getSubject());
        if (diff != 0)
            return diff;
        diff = type.compareTo(grade.getSubject());
        return diff != 0 ? diff : this.grade.compareTo(grade.getGrade());
    }

    /**
     * Method to print the grade
     * Could be used to save the grades in a csv file
     *
     * @return all values with semicolons between them
     */
    @Override
    public String toString() {
        return DATE_FORMAT.format(date) + ";" + subject + ";" + type + ";" + grade;
    }

    /**
     * Method to change the values in the class
     * Gets used if the user changes a value in the UI
     * @param grade new grade
     * @throws ParseException
     */
    public void changeValues(Grade grade) throws ParseException {
        this.date = DATE_FORMAT.parse(grade.getDate());
        this.subject = grade.getSubject();
        this.type = grade.getType();
        this.grade = grade.getGrade();

    }

    /**
     * Method to see if a grade is empty and no data is stored in it
     * @return boolean if the grade is used
     */
    public boolean isEmpty() {
        return (grade == null || grade.length() == 0) && (type == null || type.length() == 0) && (subject == null || subject.length() == 0);
    }
}
